import unittest
from cpem import CPEMatcher

matcher = CPEMatcher()


def description_search(description):
    result = matcher.search(description=description)
    for r in result:
        print(r)


class CPEMTest(unittest.TestCase):

    @unittest.skip
    def test_print(self):
        matcher.print(jp=True)

    def test_intel_search(self):
        description_search('Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz')

    def test_microsoft_search(self):
        description_search('Microsoft Office 2003')

    def test_java_search(self):
        description_search('Java JRE 1.8')

    def test_firebird_search(self):
        description_search('Firebird 2.1.1.17910 (Win32)')