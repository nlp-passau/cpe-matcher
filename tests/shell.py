from cpem import CPEMatcher, to_string_cpe
import logging

print('::: Searching for CPE :::')
logging.basicConfig(level=logging.DEBUG)

matcher = CPEMatcher()

while True:
    name = input('>_ ')
    if name == '-exit':
        break

    result = matcher.search(description=name)
    for r in result:
        print(to_string_cpe(r))

print('bye :)')
