import os
import gzip
import xmltodict
import tempfile
import requests
import collections
from cpe import CPE
import logging


OFFICIAL_CPE_DICTIONARY = 'https://nvd.nist.gov/feeds/xml/cpe/dictionary/official-cpe-dictionary_v2.3.xml.gz'
CPE_FILE_NAME = 'official-cpe-dictionary_v2.3.xml'
CPEM_DATA_DIR = '__cpem_content'
CURRENT_LAST_MODIFIED = 'current_last_modified'

CPE23 = collections.namedtuple('CPE23', 'wfn titles old_cpe new_cpe')


class CPEDictionaryLoader:
    def __init__(self, base_dir=None):
        self.logger = logging.getLogger(type(self).__name__)
        if base_dir is not None:
            self.base_dir = os.path.join(base_dir, CPEM_DATA_DIR)
        else:
            self.base_dir = os.path.join(tempfile.gettempdir(), CPEM_DATA_DIR)
        os.makedirs(self.base_dir, exist_ok=True)
        self.gz_23_dict_xml = os.path.join(self.base_dir, CPE_FILE_NAME)
        # TODO check permission error.

    def _fetch(self):
        self.logger.debug('checking database version...')
        self.logger.debug('checking database version...')
        last_modified_file_name = os.path.join(self.base_dir, CURRENT_LAST_MODIFIED)
        current_last_modified = None
        if os.path.isfile(last_modified_file_name):
            with open(last_modified_file_name) as f:
                current_last_modified = f.readline()

                self.logger.debug('local \'last modified\' is {}'.format(current_last_modified))
        head = requests.head(OFFICIAL_CPE_DICTIONARY, allow_redirects=True)
        if head.status_code == 200:
            last_modified = head.headers['Last-Modified']
            self.logger.debug('\'last modified\' available in the server {}'.format(last_modified))
            if last_modified != current_last_modified:
                self.logger.debug('download new database version...')
                response = requests.get(OFFICIAL_CPE_DICTIONARY)
                if response.status_code == 200:
                    content_file = open(self.gz_23_dict_xml, 'wb')
                    self.logger.debug('extracting gz file...')
                    result = gzip.decompress(response.content)
                    content_file.write(result)
                    content_file.close()

                    self.logger.debug('saving xml file...')
                    last_modified_file = open(last_modified_file_name, 'w')
                    last_modified_file.write(last_modified)
                    last_modified_file.close()
                    self.logger.debug('done! download successful!')
                else:
                    self.logger.debug('error retrieving the dictionary file')
        else:
            self.logger.debug('error retrieving the headers')

    def load(self, download_new=True):
        if download_new:
            self._fetch()
            self.logger.debug('loading the database...')
        if os.path.isfile(self.gz_23_dict_xml):
            with open(self.gz_23_dict_xml, 'rb') as f:
                self.logger.debug('transform xml into dict...')
                raw_data = xmltodict.parse(xml_input=f)
                cpes_list = []

                self.logger.debug('reading items...')
                for item in raw_data['cpe-list']['cpe-item']:
                    old_cpe = item['@name']
                    new_cpe = item['cpe-23:cpe23-item']['@name']
                    titles = {}

                    if not isinstance(item['title'], list):
                        item_titles = [item['title']]
                    else:
                        item_titles = item['title']

                    for t in item_titles:
                        titles[t['@xml:lang']] = t['#text']

                    wfn = CPE(new_cpe)
                    cpes_list.append(CPE23(wfn, titles, old_cpe, new_cpe))
                self.logger.debug('    - {} CPE items were loaded'.format(len(cpes_list)))
                return CPEDictionary(raw_data['cpe-list']['generator'], cpes_list)
        else:
            self.logger.debug('no dictionary file available')
            return None


class CPEDictionary:
    def __init__(self, metadata, cpes_list):
        self.metadata = metadata
        self.cpes = dict()
        self.by_vendors = dict()

        for cpe in cpes_list:
            self.cpes[cpe.new_cpe] = cpe
            vendor = cpe.wfn.get_vendor()[0]
            if vendor not in self.by_vendors:
                self.by_vendors[vendor] = list()
            self.by_vendors[vendor].append(cpe)


def to_string_cpe(cpe, wfn=False, lang='en-US'):
    return '{} ||| {}'.format(cpe.titles[lang], cpe.new_cpe)

