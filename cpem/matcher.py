from cpem.cpe_dictionary import CPEDictionaryLoader
import stringdist


class CPEMatcher:
    def __init__(self, default_lang='en-US'):
        loader = CPEDictionaryLoader()
        self.cpe_dictionary = loader.load()
        self.default_lang = default_lang

    def im_lucky(self, description=None, wfn=None):
        lang = self.__get_lang(wfn)
        result = self.search(description, wfn)
        return result[0] if len(result) > 0 else []

    def search(self, description=None, wfn=None):
        lang = self.__get_lang(wfn)
        if description is not None:
            clean_description = clean(description)
            return self._search_by_description(clean_description, lang)
        elif wfn is not None:
            return get_top(self.cpe_dictionary.by_vendors.keys(), wfn.vendor, stringdist.levenshtein)
        raise Exception('provide a description or wfn.')

    def _search_by_description(self, description, lang):
        scores = {}

        for cpe in self.cpe_dictionary.cpes.values():
            scores[cpe.new_cpe] = stringdist.levenshtein(cpe.titles[lang], description)

        scores_sorted = [(k, scores[k]) for k in sorted(scores, key=scores.get, reverse=False)]
        return [self.cpe_dictionary.cpes[key[0]] for key in scores_sorted[:10]]

    def __get_lang(self, wfn):
        language = None if wfn is None else wfn.get_language()[0]
        return self.default_lang if language is None or language == '*' else language


REMOVE = ['(R)', '(TM)', '(Win32)']


def clean(text):
    result = text
    for r in REMOVE:
        result = result.replace(r, ' ')
    return result


def get_top(target_set, seed, func, top=10):
    scores = {t : func(t, seed) for t in target_set}
    scores_sorted = [(k, scores[k]) for k in sorted(scores, key=scores.get, reverse=False)]
    return scores_sorted[:top]
