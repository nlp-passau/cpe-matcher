from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='cpem',
    version='0.0.5',
    packages=['cpem', 'tests'],
    url='gitlab.com/nlp-passau/cpe-matcher',
    license='BSD 3-Clause License',
    author='Juliano Efson Sales',
    description='Matching descriptions to CPE',
    install_requires=requirements
)
